/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Erick Rodrigues de Santana
  NUSP: 11222008

  IMDB: filmes.c


  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:
  - função mallocc retirada de: http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/


/*----------------------------------------------------------*/
/* filmes.h e a interface para as funcoes neste modulo      */
#include "filmes.h" 

/*----------------------------------------------------------*/
#include <stdlib.h>  /* NULL, free() */
#include <stdio.h>   /* printf(), scanf() */ 
#include <string.h>  /* strlen(), strncpy(), strcmp(), strtok() */

#include "util.h"    /* Bool, mallocSafe() */
#include "iofilmes.h"
#include "st.h"      /* freeST(), initST(), putFilmeST(), getFilmeST(),
                        showST(), freeST() */

/*----------------------------------------------------------------------
 *  crieFilme
 *
 *  Recebe informacoes dobre um filme 
 *
 *      - DIST  : distribuicao de notas
 *      - VOTOS : numero de votos
 *      - NOTA  : nota do filme 
 *      - ANO   : ano de producao do filme
 *
 *  e cria uma celula do tipo Filme para armazenar essa informacao. 
 *  A funcao retorna o endereco da celula criada.
 */
Filme * crieFilme (char dist[], int votos, float nota, char *nome, int ano) {
    Filme *flm;
    int len = strlen(nome) + 1; /* +1 para o '\0' */
    
    flm = mallocSafe(sizeof *flm);
    
    strncpy(flm->dist, dist, TAM_DIST+1); /* +1 para o '\0' */
    
    flm->votos = votos;
    flm->nota  = nota;
    
    flm->nome = mallocSafe(len*sizeof(char));
    strncpy(flm->nome, nome, len);
    
    flm->ano  = ano;

    flm->prox = flm->ant = NULL; /* paranoia */
    
    return flm;
}

/*----------------------------------------------------------------------
 *  crieListaFilmes
 * 
 *  Cria uma estrutura que representa lista de filmes vazia.
 *  A funcao retorna o endereco dessa estrutura.
 *
 *  Um lista de filmes e representada por uma lista encadeada 
 *  duplamente ligada com cabeca. 
 */
ListaFilmes * crieListaFilmes() {
    ListaFilmes * lista;
    
    lista = mallocSafe(sizeof * lista);
    lista->nFilmes = 0;

    lista->cab = mallocSafe(sizeof * lista->cab);
    lista->cab->nome = NULL;
    lista->cab->ant = lista->cab;
    lista->cab->prox = lista->cab;
    
    return lista;
}

/*----------------------------------------------------------------------
 *  libereListaFilmes(lst)
 *
 *  Recebe um ponteiro lst para uma estrutura que representa uma lista 
 *  de filmes e libera toda a memoria alocada para a lista.
 *
 *  Esta funcao utiliza a funcao libereFilme().
 */

void libereListaFilmes(ListaFilmes *lst) {
    Filme * flm;
    flm = lst->cab->prox;

    while (flm != lst->cab) {
        flm = flm->prox;
        libereFilme(flm->ant);
    }
    
    libereFilme(flm);
    free(lst);

    /*AVISO(libereListaFilmes em filmes.c: Vixe! Ainda nao fiz essa funcao...);*/
}

/*----------------------------------------------------------------------
 *  libereFilme
 *
 *  Recebe um ponteiro FLM para uma estrutura que representa um 
 *  filme e libera a area alocada.
 *
 */
void libereFilme(Filme *flm) {
    if (flm->nome)
        free(flm->nome);
    free(flm);
    /*AVISO(libereFilme em filmes.c: Vixe! Ainda nao fiz essa funcao...);*/
}

/*----------------------------------------------------------------------
 *  insiraFilme
 *
 *  Recebe um ponteito LST para uma estrutura que representa
 *  uma lista de filmes e um ponteiro FLM para uma estrutura
 *  que representa uma filme.
 *
 *  A funcao insere o filme na lista.
 *  
 */
void insiraFilme(ListaFilmes *lst, Filme *flm) {
    if (lst->cab->prox == lst->cab) {
        flm->prox = flm->ant = lst->cab;
        lst->cab->prox = lst->cab->ant = flm;
    }
    else {
        flm->prox = lst->cab->prox;
        flm->ant = lst->cab;
        lst->cab->prox->ant = flm;
        lst->cab->prox = flm;
    }
    
    (lst->nFilmes)++;
}


/*---------------------------------------------------------------------
 *  contemFilme
 *
 *  Recebe um ponteito LST para uma estrutura que representa
 *  uma lista de filmes e um ponteiro FLM para uma estrutura
 *  que representa uma filme.
 *
 *  A funcao retorna  TRUE se o filme esta na lista e 
 *  FALSE em caso contrario. 
 *
 *  Consideramos que dois filmes f e g sao iguais se
 *
 *       - f->nome e' igual a g->nome 
 *       - f->nota e' igual a g->nota
 *       - f->ano  e' igual a g->ano 
 *
 *  Para comparar dois nomes voce pode usar alguma funcao da 
 *  bibliteca do c  como strcmp, strncmp (string,h) 
 *  ou a funcao strCmp (util.h).
 *
 */

static Bool ehIgual(Filme * f, Filme * g) {
    return strCmp(f->nome, g->nome) && f->nota == g->nota && f->ano == g->ano;
}

Bool contemFilme(ListaFilmes *lst, Filme *flm) {
    Filme * aux;

    for (aux = lst->cab->prox; aux != NULL && !ehIgual(flm, aux); aux = aux->prox);
    if (aux == NULL)
        return FALSE;

    return TRUE;
}

/*----------------------------------------------------------------------
 *  removaFilme
 *
 *  Remove da lista de filmes LST o filme apontado por FLM.
 *    
 *  Pre-condicao: a funcao supoe que o filme FLM esta 
 *                na lista LST.
 */
void removaFilme(ListaFilmes *lst, Filme *flm) {
    flm->ant->prox = flm->prox;
    flm->prox->ant = flm->ant;
    libereFilme(flm);
    (lst->nFilmes)--;
}

static void intercala(Filme *lst1, Filme * lst2, Filme *lst3, Criterio criterio) {
    Filme *ini, *fim, *aux;

    ini = lst1;
    fim = ini;

    lst1 = lst1->prox;
    lst1->ant->prox = NULL;
    lst1->ant = NULL;

    lst2 = lst2->prox;
    lst2->ant->prox = NULL;
    lst2->ant = NULL;

    lst3->ant->prox = NULL;
    lst3->ant = NULL;

    while (lst1 != NULL || lst2 != NULL) {
        if (lst1 == NULL) {
            aux = lst2;
            lst2 = lst2->prox;
        }
        else if (lst2 == NULL) {
            aux = lst1;
            lst1 = lst1->prox;
        }
        else {
            if (criterio == NOTA) {
                if (lst1->nota >= lst2->nota) {
                    aux = lst1;
                    lst1 = lst1->prox;
                }
                else {
                    aux = lst2;
                    lst2 = lst2->prox;
                }
            }
            else {
                if (strCmp(lst1->nome, lst2->nome) <= 0) {
                    aux = lst1;
                    lst1 = lst1->prox;
                }
                else {
                    aux = lst2;
                    lst2 = lst2->prox;
                }
            }
        }

        fim->prox = aux;
        aux->ant = fim;
        fim = aux;
    }

    fim->prox = lst3;
    lst3->ant = fim;
}

void mergeSort(Filme *ini, Filme *fim, Criterio criterio) {
    Filme *lista1, *lista2;
    Filme *meio;

    if (ini->prox == fim->ant)
        return;

    lista1 = ini->prox;
    lista2 = fim->ant;

    while (lista1 != lista2 && lista1->prox != lista2) {
        lista1 = lista1->prox;
        lista2 = lista2->ant;
    }
    lista2 = lista1->prox;

    meio = mallocSafe(sizeof *meio);
    meio->nome = NULL;
    meio->ant = lista1;
    lista1->prox = meio;
    meio->prox = lista2;
    lista2->ant = meio;

    mergeSort(ini, meio, criterio);
    mergeSort(meio, fim, criterio);

    lista1 = meio->ant;
    lista2 = meio->prox;
    lista1->prox = lista2;
    lista2->ant = lista1;
    libereFilme(meio);

    intercala(ini, lista1, fim, criterio);
}

/*----------------------------------------------------------------------
 *  mergeSortFilmes
 *
 *  Recebe uma lista de filmes LST e ordena a lista utilizando o
 *  algoritmo mergeSort recursivo adaptado para listas encadeadas
 *  duplamente ligadas com cabeca.
 *
 *  A funcao recebe ainda um parametro CRITERIO tal que:
 *
 *  Se CRITERIO == NOTA, entao a lista deve ser ordenada
 *      em ordem decrescente de nota.
 *
 *  Se CRITERIO == NOME, entao a lista deve ser ordenada
 *      em ordem crescente de nome (ordem alfabetica).
 *
 *  ------------------------------------------------------------------
 *  OBSERVACAO IMPORTANTE:
 *
 *  A ordenacao deve ser feita 'in-place', ou seja o conteudo das
 *  celulas _nao deve_ ser copiado, apenas os ponteiros devem ser
 *  alterados.
 *
 *  A funcao so deve utilizar espaco extra O(1).  
 *
 *  Hmmm, ok, sem levar em consideracao o espaco O(lg n) utilizado
 *  pela pilha da recursao.  Em outras palavras, a funcao pode conter
 *  apenas declaracoes de umas poucas variaveis (um vetor v[0..n]
 *  conta como n variaveis e nao vale).
 *
 *  ------------------------------------------------------------------
 *  Para ordenar por nome, veja a funcao strCmp em util.[h|c].
 */
void mergeSortFilmes(ListaFilmes *lst, Criterio criterio) {
    mergeSort(lst->cab, lst->cab, criterio);
}

/*----------------------------------------------------------------------
 *  quickSortFilmes [opcional]
 *
 *  Recebe uma lista de filmes LST e ordena a lista utilizando o
 *  algoritmo quickSort adaptado para listas encadeadas duplamente
 *  ligadas com cabeca.
 *
 *  A funcao recebe ainda um parametro CRITERIO tal que:
 * 
 *  Se CRITERIO == NOTA, entao a lista deve ser ordenada
 *      em ordem decrescente de nota.
 *
 *  Se CRITERIO == NOME, entao a lista deve ser ordenada
 *      em ordem crescente de nome (ordem alfabetica).
 *
 *  ------------------------------------------------------------------
 *  OBSERVACAO IMPORTANTE:
 *
 *  A ordenacao deve ser feita 'in-place', ou seja o conteudo das
 *  celulas _nao deve_ ser copiado, apenas os ponteiros devem ser
 *  alterados.
 *
 *  A funcao so deve utilizar espaco extra O(1).  
 *
 *  Hmmm, ok, sem levar em consideracao o espaco O(lg n) utilizado
 *  pela pilha da recursao.  Em outras palavras, a funcao pode conter
 *  apenas declaracoes de umas poucas variaveis (um vetor v[0..n]
 *  conta como n variaveis e nao vale).
 *
 *  ------------------------------------------------------------------
 *  Para ordenar por nome, veja a funcao strCmp em util.[h|c].
 */
void quickSortFilmes(ListaFilmes *lst, Criterio criterio) {
    AVISO(quickSortFilmes em filmes.c: Vixe ainda nao fiz essa funcao...);
}

/*----------------------------------------------------------------------
 *  hashFilmes [opcional]
 *
 *  Recebe uma lista de filmes LST e distribui as palavras que
 *  ocorrem nos nomes do filmes em uma tabela de dispersao 
 *  (hash table):
 *
 *     http://www.ime.usp.br/~pf/mac0122-2002/aulas/hashing.html
 *     http://www.ime.usp.br/~pf/mac0122-2003/aulas/symb-table.html
 *
 *  Antes de inserir uma palavra na tabela de dispersao todas a
 *  letras da palavra devem ser convertidas para minusculo. Com
 *  isto faremos que a busca de filmes que possuam uma dada
 *  palavra em seu nome nao seja 'case insensitive'. Para essa
 *  tarefa a funcao tolower() pode ser utilizada.
 *
 *  Esta funcao utiliza as funcoes freeST(), initST() e putFilmeST()
 *
 *  Para obter as palavras podemos escrever uma funcao peguePalavra()
 *  inspirada na funcao pegueNome do modulo lexer.c do EP3/EP4 ou
 *  ainda utilizar a funcao strtok() da biblioteca string.h:
 *  
 *      http://linux.die.net/man/3/strtok    (man page)
 *
 */
void hashFilmes(ListaFilmes *lst) {
    AVISO(hashFilmes em filmes.c: Vixe ainda nao fiz essa funcao...);
}
