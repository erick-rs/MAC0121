/*
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Erick Rodrigues de Santana

  eval.c
  Pitao I

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerado plágio.
  Exemplo:
  - função mallocc retirada de: http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* interface para as funcoes deste modulo */
#include "eval.h" 

/*------------------------------------------------------------*/
#include <stdlib.h>  /* atoi(), atof() */
#include <string.h>  /* strncmp(), strlen(), strncpy(), strcpy(), strcmp() */
#include <math.h>    /* pow() */

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES */
#include "util.h"       /* ERRO(), AVISO(), mallocSafe() */
#include "objetos.h"    /* CelObjeto, mostreObjeto(), freeObjeto()
                           mostreListaObjetos(), 
                           freeListaObjetos() */

#include "stack.h"      /* funcoes que manipulam uma pilha */ 
                        /* essa pilha sera usada para implementar 
                           a pilha de execucao */


/*------------------------------------------------------------*/
/* Protipos de funcoes auxiliares */

/*------------------------------------------------------------*/
/* Tabela com uma representacao da precedencia dos operadores
 * atraves de valores inteiros. 
 * Quanto maior o valor, maior a precedencia.
 *
 *  http://www.ime.usp.br/~pf/algoritmos/apend/precedence.html
 */
static const int precedencia[MAX_OPERADORES] = {
    /* 4 operadores relacionais com 2 simbolos  */
     4   /* "==" */ /* da esquerda para a direita */
    ,4   /* "!=" */ /* da esquerda para a direita */
    ,5   /* ">=" */ /* da esquerda para a direita */
    ,5   /* "<=" */ /* da esquerda para a direita */
    
    /* 2 operadores aritmeticos com 2 simbolos */
    ,8 /* ,"**" */ /* da direita para a esquerda */
    ,7 /* ,"//" */ /* da esquerda para a direita */
 
    /* 2 operadores relacionais com 1 simbolo */
    ,5  /* ">"  */ /* da esquerda para a direita */
    ,5  /* "<"  */ /* da esquerda para a direita */ 
    
    /* 6 operadores aritmeticos */
    ,7  /* "%" */ /* da esquerda para a direita */
    ,7  /* "*" */ /* da esquerda para a direita */
    ,7  /* "/" */ /* da esquerda para a direita */
    ,6  /* "+" */ /* da esquerda para a direita */
    ,6  /* "-" */ /* da esquerda para a direita */
    ,8  /* "_" */ /* da direita para a esquerda */
    
    /* 3 operadores logicos  */
    ,3  /* "and" */ /* da esquerda para a direita */ 
    ,2  /* "or" */ /* da esquerda para a direita */
    ,8  /* "not"  */ /* da direita para a esquerda */
    
    /* operador de indexacao */
    ,9  /* "$"  */ /* da esquerda para a direita (IGNORAR)*/

    /* atribuicao */ 
    ,1  /* "=" */ /* da direita para a esquerda EP4 */ 
}; 

/*-------------------------------------------------------------
 *  itensParaValores
 *  
 *  RECEBE uma lista ligada com cabeca INIFILAITENS representando uma
 *  fila de itens lexicos. Inicialmente, o campo VALOR de cada celula
 *  contem um string com o item lexico.
 *
 *  A funcao CONVERTE o campo VALOR de cada celula para um inteiro 
 *  ou double, como descrito a seguir. 
 *
 *  IMPLEMENTACAO
 *
 *  O campo VALOR de uma CelObjeto tem 3 subcampos:
 *
 *      - valor.vInt   (int)
 *      - valor.vFloat (float)
 *      - valor.pStr   (string)
 *
 *  Nessa conversao, o campo VALOR de cada celula recebe um valor 
 *  da seguinte maneira.
 *
 *     - Se o campo CATEGORIA da celula indica que o item e' um
 *       string representando um int (INT_STR) entao o campo 
 *       valor.vFloat deve receber esse numero inteiro. 
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e' um
 *       string representando um float (FLOAT_STR) entao o campo 
 *       valor.vFloat deve receber esse float;
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e' um
 *       string representando um Bool (BOOL_STR) entao o campo 
 *       valor.vFloat deve receber o inteiro correspondente 
 *       (False = 0, True = 1);
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e' um
 *       string representando um operador (OPER_*) entao o campo 
 *       valor.vInt deve receber o inteiro correspondente 
 *       a precedencia desse operador. 
 *       
 *       Para isto utilize o vetor PRECEDENCIA declarado antes 
 *       desta funcao. 
 *
 * Nesta funcao (e nas outras) voce pode utilizar qualquer funcao da
 * biblioteca string do C, como, por exemplo, atoi(), atof().
 *
 * Esta funcao (e todas as outras) devem 'dar' free nas estruturas que
 * deixarem de ser necessarias.
 *
 * Esse e o caso dos strings dos itens das categorias FLOAT_STR e INT_STR.  
 *
 * Os campos strings de objetos OPER_* e BOOL_STR apontam para
 * strings em tabelas definidas no modulo lexer.h.  Nesse
 * caso, tentar liberar essa memoria e' um erro.
 *
 */

void itensParaValores(CelObjeto *iniFilaItens) {
    CelObjeto *obj = iniFilaItens->prox;
    
    while (obj != NULL) {
        if (obj->categoria <= OPER_ATRIBUICAO) {
            obj->valor.vInt = precedencia[obj->categoria];
        }
        else {
            if (obj->categoria == BOOL_STR) {
                obj->valor.vFloat = strcmp(obj->valor.pStr, "True");
            }
            else {
                char * aux = obj->valor.pStr;

                if (obj->categoria == INT_STR) {
                    obj->valor.vFloat = (double) atoi(obj->valor.pStr);
                    free(aux);
                }
                else {
                    if (obj->categoria == FLOAT_STR) {
                        obj->valor.vFloat = atof(obj->valor.pStr);
                        free(aux);
                    }
                }
            }
            
            obj->categoria = FLOAT;
        }

        obj = obj->prox;
    }
    /*AVISO(eval.c: Vixe! Ainda nao fiz a funcao itensParaValores.);*/
}

/*-------------------------------------------------------------
 * eval
 * 
 * RECEBE uma lista ligada como cabeca INIPOSFIXA que representa
 * uma expressao em notacao posfixa. 
 *
 * RETORNA o endereco de uma celula com o valor da expressao.
 *
 * PRE-CONDICAO: a funcao supoe que a expressao esta' sintaticamente
 *               correta.
 *
 * IMPLEMENTACAO
 *
 * Para o calculo do valor da expressao, deve ser usada uma pilha. 
 * O endereco retornado sera' o da celula no topo no topo dessa  
 * "pilha de execucao".
 * 
 * A funcao percorre a expressao calculando os valores resultantes.
 * Para isto e' utilizada uma pilha de execucao. 
 * 
 * A implementacao das funcoes que manipulam a pilha devem ser
 * escritas no modulo stack.c.
 * 
 * O arquivo stack.h contem a interface para essas funcoes. 
 * A pilha de execucao so deve ser acessada atraves dessa interface
 * (caso contrario nao chamariamos stack.h de interface).
 * 
 * O parametro mostrePilhaExecucao indica se os valores
 * na pilha de execucao devem ser exibidos depois de qualquer 
 * alteracao.
 * 
 * Esta funcao deve "dar free" nas estruturas que deixarem de ser 
 * necessarias.
 *
 */

CelObjeto * eval(CelObjeto *iniPosfixa, Bool mostrePilhaExecucao) {
    CelObjeto *obj;
    CelObjeto *oper1 = NULL, *oper2 = NULL;
    StackNode stack;
    
    stack = stackInit();

    while (iniPosfixa->prox != NULL) {
        obj = iniPosfixa->prox;
        iniPosfixa->prox = obj->prox;

        if (obj->categoria == FLOAT) {
            stackPush(stack, obj);
        }
        else {
            if (obj->categoria == OPER_LOGICO_NOT || obj->categoria == OPER_MENOS_UNARIO) {
                oper1 = stackPop(stack);

                if (obj->categoria == OPER_LOGICO_NOT)
                    obj->valor.vFloat = (double) !(oper1->valor.vFloat);
                else
                    obj->valor.vFloat = -(oper1->valor.vFloat);

                obj->categoria = FLOAT;
                stackPush(stack, obj);
                freeObjeto(oper1);
            }
            else if (obj->categoria <= OPER_ATRIBUICAO) {
                oper2 = stackPop(stack);
                oper1 = stackPop(stack);

                switch (obj->categoria) {
                    case OPER_IGUAL:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat == oper2->valor.vFloat);
                        break;
                    case OPER_DIFERENTE:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat != oper2->valor.vFloat);
                        break;
                    case OPER_MAIOR_IGUAL:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat >= oper2->valor.vFloat);
                        break;
                    case OPER_MENOR_IGUAL:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat <= oper2->valor.vFloat);
                        break;
                    case OPER_EXPONENCIACAO:
                        obj->valor.vFloat = pow(oper1->valor.vFloat, oper2->valor.vFloat);
                        break;
                    case OPER_DIVISAO_INT:
                        obj->valor.vFloat = (double) ((int) oper1->valor.vFloat / (int) oper2->valor.vFloat);
                        break;
                    case OPER_MAIOR:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat > oper2->valor.vFloat);
                        break;
                    case OPER_MENOR:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat < oper2->valor.vFloat);
                        break;
                    case OPER_RESTO_DIVISAO:
                        obj->valor.vFloat = fmod(oper1->valor.vFloat, oper2->valor.vFloat);
                        break;
                    case OPER_MULTIPLICACAO:
                        obj->valor.vFloat = oper1->valor.vFloat * oper2->valor.vFloat;
                        break;
                    case OPER_DIVISAO:
                        obj->valor.vFloat = (double) oper1->valor.vFloat / oper2->valor.vFloat;
                        break;
                    case OPER_ADICAO:
                        obj->valor.vFloat = oper1->valor.vFloat + oper2->valor.vFloat;
                        break;
                    case OPER_SUBTRACAO:
                        obj->valor.vFloat = oper1->valor.vFloat - oper2->valor.vFloat;
                        break;
                    case OPER_LOGICO_AND:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat && oper2->valor.vFloat);
                        break;
                    case OPER_LOGICO_OR:
                        obj->valor.vFloat = (double) (oper1->valor.vFloat || oper2->valor.vFloat);
                        break;
                    default:
                        break;
                }

                obj->categoria = FLOAT;
                stackPush(stack, obj);
                freeObjeto(oper1);
                freeObjeto(oper2);
            }
        }

        if (mostrePilhaExecucao)
            stackDump(stack);
    }

    if (stackEmpty(stack)) {
        stackFree(stack);
        return NULL;
    }
    else {
        obj = stackPop(stack);
        stackFree(stack);
    }

    return obj; 
}