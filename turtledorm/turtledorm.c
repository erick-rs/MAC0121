/*    
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Erick Rodrigues de Santana
  NUSP: 11222008

  ep1.c

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função randomInteger() copiada de: 

       http://www.ime.usp.br/~pf/algoritmos/aulas/random.html
    
   - função incrementa() copiada de: 

       https://www.ime.usp.br/~cris/mac121/eps/ep1/esqueleto/tapinhas.c

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Tamanho máximo de um turtledorm */
#define MAX 128

#define ACORDADO '#'
#define DORMINDO ' '
#define TAPINHA 'T'

#define TRUE 1
#define FALSE 0

#define ENTER '\n'
#define FIM '\0'
#define ESPACO ' '

/*   P R O T Ó T I P O S  -  P A R T E  I    */
int todosDormindo(int nLin, int nCol, int tDorm[][MAX]);
int graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]);
void leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);
void mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);
void tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col);

/*   P R O T Ó T I P O S  -  P A R T E  II   */
void sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

/*   P R O T Ó T I P O S  -  P A R T E  III  */
void resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/*   F U N Ç Õ E S   A D I C I O N A I S     */
int randomInteger(int low, int high);
int validaPosicao(int lin, int col, int nLin, int nCol);
int contaTurtlesAcordados(int nLin, int nCol, int tDorm[][MAX]);
void incremente(int bin[]);
void dormeTurtles(int nLin, int nCol, int tDorm[][MAX]);
void copiaMatriz(int nLin, int nCol, int tDorm[][MAX], int tDormAux[][MAX]);


int main(int argc, char *argv[]) {
    int tDorm[MAX][MAX], nLin, nCol, nTapinhas, lin, col;
    char lerOuSortear, decisao[3];

    printf("Digite 's' para (s)ortear ou 'l' para (l)er um turtledorm de arquivo: ");
    scanf("%c", &lerOuSortear);

    if (lerOuSortear == 'l')
        leiaTurtledorm(&nLin, &nCol, tDorm);
    else
        sorteieTurtledorm(&nLin, &nCol, tDorm);
    
    printf("Turtledorm inicial:\n");
    mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);

    printf("\nUm tapinha é definido por dois inteiros lin e col, onde\n1 <= lin <= %d e 1 <= col <= %d\n", nLin, nCol);
    printf("\n----------------------------------------------\n");
    nTapinhas = 0;

    while (!todosDormindo(nLin, nCol, tDorm)) {
        printf("Digite:\n");
        printf("--> 'd' para (d)esistir,\n");
        printf("--> 'a' para receber (a)juda para encontrar uma solução,\n");
        printf("--> 'g' para (g)ravar o turtledorm atual em um arquivo, ou\n");
        printf("--> 'lin col' para dar um tapinha na posicao[lin][col].\n");
        printf(">>> ");
        scanf("%s", decisao);

        if (decisao[0] == 'd') {
            break;
        } else if (decisao[0] == 'a') {
            resolvaTurtledorm(nLin, nCol, tDorm);
        } else if (decisao[0] == 'g') {
            graveTurtledorm(nLin, nCol, tDorm);
        } else {
            lin = atoi(decisao);
            scanf("%d", &col);
            tapinhaTurtle(nLin, nCol, tDorm, lin-1, col-1);
            nTapinhas++;
            printf("\nTurtledorm após %d tapinhas:\n", nTapinhas);
            mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);
            printf("\n");
        }
    }

    printf("\nFIM DE JOGO\n");
    printf("\nNúmero de tapinhas: %d\n", nTapinhas);

    return 0;
}

/* I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   D A   P A R T E  I */
/* falta colocar uma mensagem de erro e pedir outra vez o nome do arquivo */

/* Recebe o endereço */
void leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]) {
    int i, j;
    char nomeArquivo[80];
    FILE * arquivo;

    printf("Digite o nome do arquivo para de onde carregar o turtledorm: ");
    scanf("%s", nomeArquivo);
    arquivo = fopen(nomeArquivo, "r");

    if (arquivo == NULL) {
        printf("Não foi possível encontrar o arquivo!\n");
        exit(EXIT_FAILURE);
    }

    fscanf(arquivo, "%d %d", nLin, nCol);
    for (i = 0; i < (*nLin); i++)
        for (j = 0; j < (*nCol); j++)
            fscanf(arquivo, "%d", &(tDorm[i][j]));
    
    fclose(arquivo);
} 

void mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c) {
    int i, j;
    
    printf("\n     ");
    for (j = 1; j <= nCol; j++)
        printf("%3d   ", j);
    
    printf("\n    ");
    for (j = 1; j <= nCol; j++)
        printf("+-----");

    printf("+\n");
    for (i = 1; i <= nLin; i++) {
        printf("%3d |", i);
        for (j = 1; j <= nCol; j++) {
            if (tDorm[i-1][j-1] == 1)
                printf("  %c  ", c); 
            else
                printf("     ");
                
            printf("|");
        }
        
        printf("\n    ");
        for (j = 1; j <= nCol; j++)
            printf("+-----");

        printf("+\n");
    }
    printf("\n");
}

/* Verifica se a posição que está sendo acessada (lin col) 
está dentro da matriz nLin por nCol */
int validaPosicao(int lin, int col, int nLin, int nCol) {
    if (lin >= 0 && lin <= nLin-1 && col >= 0 && col <= nCol-1)
        return TRUE;
    
    return FALSE;
}

/* */
void tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col) {
    tDorm[lin][col] = !tDorm[lin][col];

    if (validaPosicao(lin-1, col, nLin, nCol)) /* cima */
        tDorm[lin-1][col] = !tDorm[lin-1][col];
    if (validaPosicao(lin, col+1, nLin, nCol)) /* direita */
        tDorm[lin][col+1] = !tDorm[lin][col+1];
    if (validaPosicao(lin+1, col, nLin, nCol)) /* baixo */
        tDorm[lin+1][col] = !tDorm[lin+1][col];
    if (validaPosicao(lin, col-1, nLin, nCol)) /* esquerda */
        tDorm[lin][col-1] = !tDorm[lin][col-1];
} 

/* Recebe o número de linhas e colunas e a matriz tDorm
e retorna TRUE se todos os elementos da matriz forem 0 */
int todosDormindo(int nLin, int nCol, int tDorm[][MAX]) {
    int i, j;
    for (i = 0; i < nLin; i++)
        for (j = 0; j < nCol; j++)
            if (tDorm[i][j] != 0)
                return FALSE;
    
    return TRUE;
}

/* Recebe a matriz tDorm nLin por nCol e salva o seu estado atual
em um arquivo. Retorna EXIT_SUCCESS se a gravação foi feita com
sucesso ou EXIT_FAILURE caso contrário. */
int graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]) {
    int i, j;
    char nome[100];
    FILE *arquivo;

    printf("Digite o nome do arquivo onde salvar o turtledorm: ");
    scanf("%s", nome);

    arquivo = fopen(nome, "w");

    if (arquivo == NULL)
        return EXIT_FAILURE;
    
    fprintf(arquivo, "%d", nLin);
    putc(ESPACO, arquivo);
    fprintf(arquivo, "%d", nCol);
    putc(ENTER, arquivo);

    for (i = 0; i < nLin; i++) {
        for (j = 0; j < nCol; j++) {
            putc('0' + tDorm[i][j], arquivo);
            putc(ESPACO, arquivo);
        }

        putc(ENTER, arquivo);
    }
            
    fclose(arquivo);

    return EXIT_SUCCESS;
}


/* I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   D A   P A R T E  II */
/* Coloca todos os turtles para dormir (zera todas as posições da matriz nLin por nCol) */
void dormeTurtles(int nLin, int nCol, int tDorm[][MAX]) {
    int i, j;
    for (i = 0; i < nLin; i++)
        for (j = 0; j < nCol; j++)
            tDorm[i][j] = 0;
}

int contaTurtlesAcordados(int nLin, int nCol, int tDorm[][MAX]) {
    int i, j, nTurtlesAcordados = 0;

    for (i = 0; i < nLin; i++)
        for (j = 0; j < nCol; j++)
            if (tDorm[i][j] == 1)
                nTurtlesAcordados++;
    
    return nTurtlesAcordados;
}

/* A função randomInteger devolve um inteiro 
aleatório entre low e high inclusive,
ou seja, no intervalo fechado low..high.
Vamos supor que low <= high e que
high - low <= RAND_MAX. (O código foi copiado
da biblioteca random de Eric Roberts.) */
int randomInteger(int low, int high) {
    int k;
    double d;
    d = (double) rand() / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + k;
}

/* Recebe os endereços de nLin e nCol e a matriz tDorm, sorteando*/
void sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]) {
    int semente, low, high, nTapinhas, nTurtlesAcordados, i, lin, col;
    char dificuldade;

    printf("Digite a dimensão do turtledorm (nLin nCol): ");
    scanf("%d %d", nLin, nCol);
    printf("Digite um inteiro para o gerador de números aleatórios (semente): ");
    scanf("%d", &semente);
    printf("Digite o nível de dificuldade [f/m/d]: ");
    scanf(" %c", &dificuldade);

    dormeTurtles(*nLin, *nCol, tDorm);
    srand(semente);

    if (dificuldade == 'f') {
        low = (int) (0.05 * (*nLin) * (*nCol));
        high = (int) (0.20 * (*nLin) * (*nCol));
    } else if (dificuldade == 'm') {
        low = (int) (0.25 * (*nLin) * (*nCol));
        high = (int) (0.50 * (*nLin) * (*nCol));
    } else {
        low = (int) (0.55 * (*nLin) * (*nCol));
        high = (int) (0.85 * (*nLin) * (*nCol));
    }
    
    nTapinhas = randomInteger(low, high);
    for (i = 0; i < nTapinhas; i++) {
        lin = randomInteger(0, (*nLin)-1);
        col = randomInteger(0, (*nCol)-1);
        tapinhaTurtle(*nLin, *nCol, tDorm, lin, col);
    }

    nTurtlesAcordados = contaTurtlesAcordados(*nLin, *nCol, tDorm);
    printf("\nNúmero de tapinhas sorteado = %d", nTapinhas);
    printf("\nNúmero de turtles despertos = %d\n\n", nTurtlesAcordados);
}


/* I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   D A   P A R T E  III */
void copiaMatriz(int nLin, int nCol, int tDorm[][MAX], int tDormAux[][MAX]) {
    int i, j;

    for (i = 0; i < nLin; i++)
        for (j = 0; j < nCol; j++)
            tDormAux[i][j] = tDorm[i][j];
}

/* 
 * incremente(bin)
 * 
 * Recebe atraves do vetor BIN a representacao de um 
 * número binario k e devolve em BIN a representacao 
 * binaria de k+1.
 * 
 * Pre-condicao: a funcao nao se preocupa com overflow,
 *   ou seja, supoe que k+1 pode ser representado em 
 *   BIN.
 */ 
void incremente(int bin[]) {
    int i;

    for (i = 0; bin[i] != 0; i++)
        bin[i] = 0;

    bin[i] = 1;
}

void resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]) {
    int i, j, nBits, nSolucoes, nTapinhas, menorTapinhas, estaoDormindo, temSolucao;
    int tDormAux[MAX][MAX], bin[MAX+1], tapinhas[MAX][MAX], tapinhasMenor[MAX][MAX];

    nTapinhas = nSolucoes = 0;
    menorTapinhas = -1;
    nBits = nCol;
    estaoDormindo = temSolucao = FALSE;

    for (i = 0; i < nBits+1; i++)
        bin[i] = 0;

    while (bin[nBits] == 0) {
        copiaMatriz(nLin, nCol, tDorm, tDormAux);
        dormeTurtles(nLin, nCol, tapinhas);
        
        for (j = 0; j < nBits; j++) {
            if (bin[j] == 1) {
                nTapinhas++;
                tapinhas[0][j] = 1;
                tapinhaTurtle(nLin, nCol, tDormAux, 0, j);
            }
        }
        for (i = 1; i < nLin; i++) {
            for (j = 0; j < nCol; j++) {
                if (tDormAux[i-1][j] == 1) {
                    nTapinhas++;
                    tapinhas[i][j] = 1;
                    tapinhaTurtle(nLin, nCol, tDormAux, i, j);
                }
            }
        }

        estaoDormindo = todosDormindo(nLin, nCol, tDormAux);
        if (estaoDormindo == TRUE) {
            temSolucao = TRUE;
            nSolucoes++;
            if (menorTapinhas == -1) {
                menorTapinhas = nTapinhas;
                copiaMatriz(nLin, nCol, tapinhas, tapinhasMenor);
            }
            else {
                if (nTapinhas < menorTapinhas) {
                    menorTapinhas = nTapinhas;
                    copiaMatriz(nLin, nCol, tapinhas, tapinhasMenor);
                }
            }
        }
        nTapinhas = 0;
        incremente(bin);
    }

    if (temSolucao == FALSE)
        printf("\nNÃO TEM SOLUÇÃO!\n");
    else {
        printf("\nSOLUÇÃO MENOS VIOLENTA\n");
        printf("Foram encontrada(s) %d solução(ões)\n", nSolucoes);
        printf("\nA SOLUÇÃO MENOS VIOLENTA TEM %d TAPINHAS\n", menorTapinhas);
        mostreTurtledorm(nLin, nCol, tapinhasMenor, TAPINHA);
    }
}
