/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Erick Rodrigues de Santana

  posfixa.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função mallocc retirada de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* iterface para o uso da funcao deste módulo */
#include "posfixa.h"

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES, INDEFINIDA, 
                           ABRE_PARENTESES, ... */
#include "objetos.h" /* tipo CelObjeto, freeObjeto(), freeListaObjetos() */
#include "stack.h"   /* stackInit(), stackFree(), stackPop() 
                        stackPush(), stackTop() */


void alteraApontadores(CelObjeto **, CelObjeto **);
void queuePut(CelObjeto**, CelObjeto**);

/*-------------------------------------------------------------
 *  infixaParaPosfixa
 * 
 *  Recebe uma lista ligada com cabeca INIINFIXA representando uma
 *  fila de itens de uma expressao infixa e RETORNA uma lista ligada
 *  com cabeca contendo a fila com a representacao da correspondente
 *  expressao em notacao posfixa.
 */
 /*  As celulas da notacao infixa que nao forem utilizadas na notacao 
  *  posfixa (abre e fecha parenteses) devem ser liberadas 
  *  (freeObjeto()).
  */

CelObjeto * infixaParaPosfixa(CelObjeto *iniInfixa) {
    CelObjeto *iniPosfixa, *fimPosfixa, *obj, *nova, *aux, *pCel;
	StackNode stack;
	
	fimPosfixa = iniPosfixa = iniInfixa;
    stack = stackInit();
	obj = iniInfixa->prox;

    while (obj != NULL) {
		switch (obj->categoria) {
			case FLOAT:
			case ID:
				alteraApontadores(&nova, &obj);
				queuePut(&fimPosfixa, &nova);
				break;
			case ABRE_PARENTESES:
				alteraApontadores(&aux, &obj);
				stackPush(stack, aux);
				break;
			case FECHA_PARENTESES:
				for (pCel = stackPop(stack); pCel->categoria != ABRE_PARENTESES; pCel = stackPop(stack)) {
					nova = pCel;
					queuePut(&fimPosfixa, &nova);
				}
				alteraApontadores(&aux, &obj);
				freeObjeto(pCel);
				freeObjeto(aux);
				break;
			case OPER_MENOS_UNARIO:
			case OPER_LOGICO_NOT:
			case OPER_EXPONENCIACAO:
			case OPER_ATRIBUICAO:
				/* Operadores da direita pra esquerda: tiram da pilha e colocam na fila os operadores
				de precedencia MAIOR que a dele */
				if (!stackEmpty(stack)) pCel = stackTop(stack);
				while (!stackEmpty(stack) && pCel->categoria != ABRE_PARENTESES && pCel->prec > obj->prec) {
					nova = stackPop(stack);
					queuePut(&fimPosfixa, &nova);
					if (!stackEmpty(stack)) pCel = stackTop(stack);
				}
				alteraApontadores(&aux, &obj);
				stackPush(stack, aux);
				break;
			default:
				/* Operadores da esquerda pra direta: tiram da pilha e colocam na fila os operadores
				de precedencia MAIOR OU IGUAL que a dele */
				if (!stackEmpty(stack)) pCel = stackTop(stack);
				while (!stackEmpty(stack) && pCel->categoria != ABRE_PARENTESES && pCel->prec >= obj->prec) {
					nova = stackPop(stack);
					queuePut(&fimPosfixa, &nova);
					if (!stackEmpty(stack)) pCel = stackTop(stack);
				}
				alteraApontadores(&aux, &obj);
				stackPush(stack, aux);
				break;
		}
	}

	/* Coloca na fila os operadores restantes */
	while (!stackEmpty(stack)) {
		nova = stackPop(stack);
		queuePut(&fimPosfixa, &nova);
	}

	stackFree(stack);

    return iniPosfixa; 
}

/* As funções abaixo reduzem a repetição de código */
void alteraApontadores(CelObjeto ** aux, CelObjeto ** obj) {
	(*aux) = *obj;
	(*obj) = (*obj)->prox;
}

void queuePut(CelObjeto ** fimPosfixa, CelObjeto ** nova) {
	(*fimPosfixa)->prox = *nova;
	(*fimPosfixa) = *nova;
}
