/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÃ‡ALHO COM O MEU NOME E O MEU NÃšMERO USP, 
  DECLARO QUE SOU O ÃšNICO AUTOR E RESPONSÃVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÃCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÃ‡Ã•ES DESSE EP
  E QUE PORTANTO NÃƒO CONSTITUEM PLÃGIO. DECLARO TAMBÃ‰M QUE SOU RESPONSÃVEL
  POR TODAS AS CÃ“PIAS DESSE PROGRAMA E QUE EU NÃƒO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÃ‡ÃƒO. ESTOU CIENTE QUE OS CASOS DE PLÃGIO SÃƒO PUNIDOS COM 
  REPROVAÃ‡ÃƒO DIRETA NA DISCIPLINA.

  Nome: Erick Rodrigues de Santana

  eval.c
  Pitao II

  ReferÃªncias: Com exceÃ§Ã£o das rotinas fornecidas no esqueleto e em sala
  de aula, caso vocÃª tenha utilizado alguma refÃªncia, liste-as abaixo
  para que o seu programa nÃ£o seja considerada plÃ¡gio.
  Exemplo:
  - funÃ§Ã£o mallocc retirada de: http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* interface para as funcoes deste modulo */
#include "eval.h" 

/*------------------------------------------------------------*/
#include <stdlib.h>  /* atoi(), atof(), strtol() */
#include <string.h>  /* strncmp(), strlen(), strncpy(), strcpy(), strcmp() */
#include <math.h>    /* pow() */

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES */
#include "util.h"       /* ERRO(), AVISO(), mallocSafe() */
#include "objetos.h"    /* CelObjeto, mostreObjeto(), freeObjeto()
                           mostreListaObjetos(), 
                           freeListaObjetos() */
#include "stack.h"      /* funcoes que manipulam uma pilha */ 
                        /* essa pilha sera usada para implementar 
                           a pilha de execucao */
#include "st.h"         /* getValorST(), setValorST() */    
    

/*------------------------------------------------------------*/
/* Protipos de funcoes auxiliares */

/*------------------------------------------------------------*/
/* Tabela com uma representacao da precedencia dos operadores
 * atraves de valores inteiros. 
 * Quanto maior o valor, maior o valor, maior a precedencia.
 *
 *  http://www.ime.usp.br/~pf/algoritmos/apend/precedence.html
 */
static const int precedencia[MAX_OPERADORES] =
{
    /* 4 operadores relacionais com 2 simbolos  */
    4   /* "==" */ /* da esquerda para a direita */
    ,4   /* "!=" */ /* da esquerda para a direita */
    ,5   /* ">=" */ /* da esquerda para a direita */
    ,5   /* "<=" */ /* da esquerda para a direita */
         
    /* 2 operadores aritmeticos com 2 simbolos */
    ,8 /* ,"**" */ /* da direita para a esquerda */
    ,7 /* ,"//" */ /* da esquerda para a direita */
 
    /* 2 operadores relacionais com 1 simbolo */
    ,5  /* ">"  */ /* da esquerda para a direita */
    ,5  /* "<"  */ /* da esquerda para a direita */ 
    
    /* 6 operadores aritmeticos */
    ,7  /* "%" */ /* da esquerda para a direita */
    ,7  /* "*" */ /* da esquerda para a direita */
    ,7  /* "/" */ /* da esquerda para a direita */
    ,6  /* "+" */ /* da esquerda para a direita */
    ,6  /* "-" */ /* da esquerda para a direita */
    ,8  /* "_" */ /* da direita para a esquerda */
    
    /* 3 operadores logicos  */
    ,3  /* "and" */ /* da esquerda para a direita */ 
    ,2  /* "or" */ /* da esquerda para a direita */
    ,8  /* "not"  */ /* da direita para a esquerda */
    
    /* operador de indexacao */
    ,9  /* "$"  */ /* da esquerda para a direita (IGNORAR)*/

    /* atribuicao */ 
    ,1  /* "=" */ /* da direita para a esquerda EP4 */ 
}; 

/*-------------------------------------------------------------
 *  itensParaValores
 *  
 *  RECEBE uma lista ligada com cabeca INIFILAITENS representando uma
 *  fila de itens lexicos. Inicialmente, o campo VALOR de cada celula
 *  contem um string com o item lexico.
 *
 *  A funca CONVERTE o campo VALOR de cada celula para um inteiro 
 *  ou double, como descrito a seguir. 
 *
 *  IMPLEMENTACAO
 *
 *  O campo VALOR de uma CelObjeto tem 3 subcampos:
 *
 *      - valor.vInt   (int)
 *      - valor.vFloat (float)
 *      - valor.pStr   (string)
 *
 *  Nessa conversao, o campo VALOR de cada celula recebe um valor 
 *  da seguinte maneira.
 *
 *     - Se o campo CATEGORIA da celula indica que o item e um
 *       string representando um int (INT_STR) entao o campo 
 *       valor.vFloat deve receber esse numero inteiro. 
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e um
 *       string representando um float (FLOAT_STR) entao o campo 
 *       valor.vFloat deve receber esse float;
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e um
 *       string representando um Bool (BOOL_STR) entao o campo 
 *       valor.vFloat deve receber o inteiro correspondente 
 *       (False = 0, True = 1);
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e um
 *       string representando um operador (OPER_*) entao o campo 
 *       valor.vInt deve receber o inteiro correspondente 
 *       a precedencia desse operador. 
 *       
 *       Para isto utilize o vetor PRECEDENCIA declarado antes 
 *       desta funcao. 
 *
 * Nesta funcao (e nas outras) voce pode utilizar qualquer funcao da
 * biblioteca string do C, como, por exemplo, atoi(), atof().
 *
 * Esta funcao (e todas as outras) devem 'dar' free nas estruturas que
 * deixarem de ser necessarias.
 *
 * Esse e o caso das dos strings dos itens das categorias 
 * FLOAT_STR e INT_STR.  
 *
 * Os campos strings de objetos OPER_* e BOOL_STR apontam para
 * strings em tabelas definidas no modulo lexer.h. Nesse
 * caso, tentar liberar essa memoria e' um erro.
 *
 */

void itensParaValores(CelObjeto *iniFilaItens) {
    CelObjeto *obj = iniFilaItens->prox;
    
    while (obj != NULL) {
        if (obj->categoria <= OPER_ATRIBUICAO) {
            obj->valor.vInt = precedencia[obj->categoria];
        }
        else {
            if (obj->categoria == BOOL_STR) {
                obj->valor.vFloat = strcmp(obj->valor.pStr, "True");
                obj->categoria = FLOAT;
            }
            else {
                if (obj->categoria == INT_STR) {
                    char * aux = obj->valor.pStr;
                    obj->valor.vFloat = (double) atoi(obj->valor.pStr);
                    obj->categoria = FLOAT;
                    free(aux);
                }
                else {
                    if (obj->categoria == FLOAT_STR) {
                        char * aux = obj->valor.pStr;
                        obj->valor.vFloat = atof(obj->valor.pStr);
                        obj->categoria = FLOAT;
                        free(aux);
                    }
                }
            }
        }

        obj = obj->prox;
    }
}

/*-------------------------------------------------------------
 * eval
 * 
 * RECEBE uma lista ligada como cabeca INIPOSFIXA que representa
 * uma expressao em notacao posfixa. 
 *
 * RETORNA o endereco de uma celula com o valor da expressao.
 *
 * PRE-CONDICAO: a funcao supoe que a expressao esta sintaticamente
 *               correta.
 *
 * IMPLEMENTACAO
 *
 * Para o calculo do valor da expressao deve ser usada uma 
 * pilha. O endereco retornado sera o da celula no topo no
 * topo dessa  "pilha de execucao".
 * 
 * A funcao percorre a expressao calculando os valores resultantes.
 * Para isto e utilizada uma pilha de execucao. 
 * 
 * A implementacao das funcoes que manipulam a pilham devem ser
 * escritas no modulo stack.c.
 * 
 * O arquivo stack.h contem a interface para essas funcoes. 
 * A pilha de execucao so deve ser acessada atraves dessa interface
 * (em caso contrario nao chamariamos stack.h de interface).
 * 
 * O parametro mostrePilhaExecucao indica se os valores
 * na pilha de execucao devem ser exibidos depois de qualquer 
 * alteracao.
 * 
 * Esta funcao deve "dar free" nas estruturas que deixarem de ser 
 * necessarias.
 *
 * EP4: funcao deve ser adaptada para tratar do operador de atribuicao
 *      '=' e variaveis. A maneira de proceder esta descrita no 
 *      enunciado na secao "Interpretacao da expressao posfixa".
 */
CelObjeto * eval(CelObjeto *iniPosfixa, Bool mostrePilhaExecucao) {
    CelObjeto *obj, *aux;
    CelObjeto *pCel1 = NULL, *pCel2 = NULL;
    StackNode stack;
    
    stack = stackInit();

    while (iniPosfixa->prox != NULL) {
        obj = iniPosfixa->prox;
        iniPosfixa->prox = obj->prox;

        if (obj->categoria == ID || obj->categoria == FLOAT) {
            stackPush(stack, obj);
        }
        else {
            if (obj->categoria == OPER_LOGICO_NOT || obj->categoria == OPER_MENOS_UNARIO) {
                pCel1 = stackPop(stack);

                if (obj->categoria == OPER_LOGICO_NOT)
                    obj->valor.vFloat = (double) !(pCel1->valor.vFloat);
                else
                    obj->valor.vFloat = -(pCel1->valor.vFloat);

                obj->categoria = FLOAT;
                stackPush(stack, obj);
                freeObjeto(pCel1);
            }
            else if (obj->categoria < OPER_ATRIBUICAO) {
                pCel2 = stackPop(stack);
                pCel1 = stackPop(stack);

                if (pCel2->categoria == ID) {
                    aux = pCel2;
                    pCel2 = getValorST(pCel2->nomeID);
                    freeObjeto(aux);
                }

                if (pCel1->categoria == ID) {
                    aux = pCel1;
                    pCel1 = getValorST(pCel1->nomeID);
                    freeObjeto(aux);
                }

                switch (obj->categoria) {
                    case OPER_IGUAL:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat == pCel2->valor.vFloat);
                        break;
                    case OPER_DIFERENTE:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat != pCel2->valor.vFloat);
                        break;
                    case OPER_MAIOR_IGUAL:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat >= pCel2->valor.vFloat);
                        break;
                    case OPER_MENOR_IGUAL:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat <= pCel2->valor.vFloat);
                        break;
                    case OPER_EXPONENCIACAO:
                        obj->valor.vFloat = pow(pCel1->valor.vFloat, pCel2->valor.vFloat);
                        break;
                    case OPER_DIVISAO_INT:
                        obj->valor.vFloat = (double) ((int) pCel1->valor.vFloat / (int) pCel2->valor.vFloat);
                        break;
                    case OPER_MAIOR:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat > pCel2->valor.vFloat);
                        break;
                    case OPER_MENOR:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat < pCel2->valor.vFloat);
                        break;
                    case OPER_RESTO_DIVISAO:
                        obj->valor.vFloat = fmod(pCel1->valor.vFloat, pCel2->valor.vFloat);
                        break;
                    case OPER_MULTIPLICACAO:
                        obj->valor.vFloat = pCel1->valor.vFloat * pCel2->valor.vFloat;
                        break;
                    case OPER_DIVISAO:
                        obj->valor.vFloat = (double) pCel1->valor.vFloat / pCel2->valor.vFloat;
                        break;
                    case OPER_ADICAO:
                        obj->valor.vFloat = pCel1->valor.vFloat + pCel2->valor.vFloat;
                        break;
                    case OPER_SUBTRACAO:
                        obj->valor.vFloat = pCel1->valor.vFloat - pCel2->valor.vFloat;
                        break;
                    case OPER_LOGICO_AND:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat && pCel2->valor.vFloat);
                        break;
                    case OPER_LOGICO_OR:
                        obj->valor.vFloat = (double) (pCel1->valor.vFloat || pCel2->valor.vFloat);
                        break;
                    default:
                        break;
                }

                obj->categoria = FLOAT;
                stackPush(stack, obj);
                freeObjeto(pCel1);
                freeObjeto(pCel2);
            }
            else if (obj->categoria == OPER_ATRIBUICAO) {
                pCel1 = stackPop(stack);
                pCel2 = stackPop(stack);

                if (pCel1->categoria == ID) {
                    aux = pCel1;
                    pCel1 = getValorST(pCel1->nomeID);
                    freeObjeto(aux);
                }
               
                setValorST(pCel2->nomeID, pCel1);

                obj->categoria = FLOAT;
                obj->valor.vFloat = pCel1->valor.vFloat;
                stackPush(stack, obj);

                freeObjeto(pCel1);
                freeObjeto(pCel2);
            }
        }

        if (mostrePilhaExecucao)
            stackDump(stack);
    }

    if (stackEmpty(stack)) {
        stackFree(stack);
        return NULL;
    }
    else {
        obj = stackPop(stack);
        stackFree(stack);
    }

    return obj; 
}
