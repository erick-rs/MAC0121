/*
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Erick Rodrigues de Santana

  stack.c
  Pitao I

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerado plágio.

  - todas as funções (exceto stackDump()) foram retiradas de:

    https://www.ime.usp.br/~cris/mac121/programas/polonesa/stack2/

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/* interface para o uso de uma pilha */
#include <stdlib.h>
#include <stdio.h>
#include "stack.h"
#include "util.h"

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 */

/*
 * PILHA: uma implementacao com lista encadeada com cabeca
 */

StackNode stackInit() {
	StackNode stack;
	stack = mallocSafe(sizeof *stack);
	stack->prox = NULL;
	return stack;
}

int stackEmpty(StackNode stack) {
	return stack->prox == NULL;
}

void stackPush(StackNode stack, StackNode nova) {
	nova->prox = stack->prox;
	stack->prox = nova;
}

StackNode stackPop(StackNode stack) {
	StackNode p = stack->prox;

	if (p == NULL) /* stackempty() */ {
		fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
		exit(-1);
	}
	/* tudo bem, a pilha nao esta vazia... */
	stack->prox = p->prox;
	p->prox = NULL;
	return p;  
}

StackNode stackTop(StackNode stack) {
	if (stack->prox == NULL) /* stackempty() */ {
		fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
		exit(-1);
	}
	/* tudo bem, a pilha nao esta vazia... */
	return stack->prox;
}

void stackFree(StackNode stack) {
	while (stack != NULL) {
		StackNode removido = stack;
		stack = removido->prox;
		free(removido);
	}
}

void stackDump(StackNode stack) {
	StackNode p = stack->prox;

	fprintf(stdout, "\n==========================\n");
	if (p == NULL) 
		fprintf(stdout, "  Pilha vazia.");
	else
		fprintf(stdout, "  Pilha de execucao\n  valor\n");
	
	fprintf(stdout, ". . . . . . . . . . . . . .\n");
	
	while (p != NULL) {
		fprintf(stdout, "  %f\n", p->valor.vFloat);
		p = p->prox;
	}

	fprintf(stdout,"\n");
}
